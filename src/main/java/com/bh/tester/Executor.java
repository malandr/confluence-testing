package com.bh.tester;

public class Executor {

    public static void main(String[] args) {

        String word = "BASICFILE";

        String html = "<ins>for LOB fields works only <b>when the LOB storage</b> option is specified as SECUREFILE and not for the existing BASICFILE storage</ins>";

        String result = HtmlToStringService.convertHTMLtoText(html);

        System.out.println(result);

        System.out.println(html.indexOf(word));

    }
}
