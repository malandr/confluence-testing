package com.bh.tester.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.mail.notification.ConversionContextCreator;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.web.context.HttpContext;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.bh.tester.services.LinksExtractorService;
import com.bh.tester.services.MarkersService;
import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Scanned
public class TesterMacro implements Macro {

    private final String TEMPLATE = "templates/macro-template.vm";

    private final PageManager pageManager;
    private final MarkersService markersService;
    private final XhtmlContent xhtmlContent;
    private final UserAccessor userAccessor;
    private final SpaceManager spaceManager;
    private final LinksExtractorService linksExtractorService;
    private final HttpContext httpContext;
    private final LabelManager labelManager;

    public TesterMacro(PageManager pageManager,
                       MarkersService markersService,
                       XhtmlContent xhtmlContent,
                       UserAccessor userAccessor,
                       SpaceManager spaceManager,
                       LinksExtractorService linksExtractorService,
                       HttpContext httpContext,
                       LabelManager labelManager) {
        this.pageManager = pageManager;
        this.markersService = markersService;
        this.xhtmlContent = xhtmlContent;
        this.userAccessor = userAccessor;
        this.spaceManager = spaceManager;
        this.linksExtractorService = linksExtractorService;
        this.httpContext = httpContext;
        this.labelManager = labelManager;
    }

    private boolean isUrlValid(String URL) {

        try {
            URL customUrl = new URL(URL);
            customUrl.toURI();
            HttpURLConnection connection = (HttpURLConnection) customUrl.openConnection();
            connection.setRequestMethod("GET");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean urlValidator(String url) {

        UrlValidator defaultValidator = new UrlValidator();

        return defaultValidator.isValid(url);
    }

    private String getBodyFromUrl(String url) {

        Document document;
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            return "Exception " + e.getMessage();
        }

        return document.title();

    }

    @Override
    public String execute(Map<String, String> params, String s, ConversionContext conversionContext) throws MacroExecutionException {

        Map<String, Object> context = MacroUtils.defaultVelocityContext();

        Page currentPage = pageManager.getPage(conversionContext.getEntity().getId());
        Space currentSpace = spaceManager.getSpace(conversionContext.getSpaceKey());

        String currPageTitle = currentPage.getTitle();
        String currSpaceKey = currentSpace.getKey();

        try {

            ConversionContextCreator conversionContextCreator = new ConversionContextCreator();

            ConversionContext context1 = conversionContextCreator.createConversionContext(currentPage);

            String viewBody = xhtmlContent.convertStorageToView(currentPage.getBodyAsString(), context1);

//            String xhtmlBody = xhtmlContent.convertWikiBodyToStorage(currentPage).getBodyAsString();
//            String viewBody = xhtmlContent.convertStorageToView(currentPage.getBodyAsString(), conversionContext);
            String body = currentPage.getBodyAsString();
            context.put("viewBody", viewBody);
//            context.put("xhtmlBody", xhtmlBody);
            context.put("body", body);

            Label label = new Label("trash_bin_page");

            List<ContentEntityObject> pagesByLabel = labelManager.getContentForLabel(1, 10, label).getList();

            for (ContentEntityObject object : pagesByLabel) {
                
            }

        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (XhtmlException e) {
            e.printStackTrace();
        }

        Date date = new Date();

        return VelocityUtils.getRenderedTemplate(TEMPLATE, context);
    }


    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
