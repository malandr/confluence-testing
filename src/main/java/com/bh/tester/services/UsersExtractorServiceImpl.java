package com.bh.tester.services;

import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.sal.api.user.UserKey;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UsersExtractorServiceImpl implements UsersExtractorService {

    private final XhtmlContent xhtmlContent;
    private final UserAccessor userAccessor;

    public UsersExtractorServiceImpl(XhtmlContent xhtmlContent, UserAccessor userAccessor) {
        this.xhtmlContent = xhtmlContent;
        this.userAccessor = userAccessor;
    }

    @Override
    public Set<ConfluenceUser> extractUsers(Comment comment) {

        String storageBody = xhtmlContent.convertWikiBodyToStorage(comment).getBodyAsString();

        List<Integer> indexes = new ArrayList<Integer>();
        List<UserKey> mentionedKeys = new ArrayList<>();
        Set<ConfluenceUser> mentioedUsers = new HashSet<>();

        int index = 0;
        while(index != -1){
            index = storageBody.indexOf("ri:userkey=\"", index);
            if (index != -1) {
                indexes.add(index);
                index++;
            }
        }

        for (Integer tesmpIndex : indexes) {
            int start = tesmpIndex + 12;
            int end = start + 32;
            String mentioedKey = storageBody.substring(start, end);
            UserKey userKey = new UserKey(mentioedKey);
            mentionedKeys.add(userKey);
        }

        for (UserKey tempKey : mentionedKeys) {
            mentioedUsers.add(userAccessor.getUserByKey(tempKey));
        }

        return mentioedUsers;
    }
}
