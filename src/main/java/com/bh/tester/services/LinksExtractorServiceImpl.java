package com.bh.tester.services;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.model.links.DefaultLink;
import com.atlassian.confluence.content.render.xhtml.model.links.DefaultLinkBuilder;
import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.PageResourceIdentifier;
import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.xhtml.api.Link;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.bh.tester.models.BhtLink;
import com.bh.tester.models.BhtModification;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Component
public class LinksExtractorServiceImpl implements LinksExtractorService {

    private final XhtmlContent xhtmlContent;
    private final PageManager pageManager;

    @Autowired
    public LinksExtractorServiceImpl(XhtmlContent xhtmlContent,
                                     PageManager pageManager) {
        this.xhtmlContent = xhtmlContent;
        this.pageManager = pageManager;
    }

    @Override
    public List<BhtLink> getWebLinksFromPage(Page page, String hostUrl) {

        String html = xhtmlContent.convertWikiBodyToStorage(page).getBodyAsString();

        List<BhtLink> webLinks = new ArrayList<>();

        Document document = Jsoup.parse(html);

        Elements links = document.getElementsByTag("a");

        for (Element link : links) {
            BhtLink bhtLink = new BhtLink();
            bhtLink.setFullUrl(link.outerHtml());
            bhtLink.setHref(link.attr("href"));
            bhtLink.setText(link.text());
            webLinks.add(bhtLink);
        }

        return webLinks;
    }

    public boolean containsWebConfLinks(Page page, String hostUrl) {

        for (BhtLink bhtLink : getWebLinksFromPage(page, hostUrl)) {
            if (bhtLink.getHref().contains(hostUrl) && bhtLink.getHref().contains("/display/")) return true;
        }
        return false;
    }

    @Override
    public void changeLinks(Page page, String hostUrl) throws UnsupportedEncodingException {

        if (containsWebConfLinks(page, hostUrl)) {

            String xhtmlBody = xhtmlContent.convertWikiBodyToStorage(page).getBodyAsString();
            ConversionContext context = new DefaultConversionContext(page.toPageContext());

            for (BhtLink bhtLink : getWebLinksFromPage(page, hostUrl)) {

                if (bhtLink.getHref().contains(hostUrl) && bhtLink.getHref().contains("/display/")) {

                    String resultSubstr;

                    if (bhtLink.getHref().contains("?")) resultSubstr = bhtLink.getHref().substring(bhtLink.getHref().indexOf("/display/"), bhtLink.getHref().indexOf("?"));
                    else resultSubstr = bhtLink.getHref().substring(bhtLink.getHref().indexOf("/display/"));

                    String[] arr = resultSubstr.split("/");
                    String spaceKey = arr[2];                                                           // TODO - check if space really exists
                    String pageTitle = arr[3].replace("+", " ");                                // TODO - Add other symbols like "+" !!!
                    String pageTitleResult;

                    if (pageTitle.contains("%")) pageTitleResult = URLDecoder.decode(pageTitle, StandardCharsets.UTF_8.name());
                    else pageTitleResult = pageTitle;


                    DefaultLinkBuilder linkBuilder = DefaultLink.builder();

                    PageResourceIdentifier identifier = new PageResourceIdentifier(spaceKey, pageTitleResult);

                    Link wiseLink = linkBuilder.withDestinationResourceIdentifier(identifier).build();

                    try {

                        String storageOfLink1 = xhtmlContent.convertLinkToStorage(wiseLink, context);

                        page.setBodyAsString(xhtmlBody.replace(bhtLink.getFullUrl(), storageOfLink1));

                    } catch (XhtmlException e) {
                        e.printStackTrace();
                    }
                }

                if (bhtLink.getHref().contains(hostUrl) && bhtLink.getHref().contains("/pages/viewpage.action?pageId=")) {

                    String subStr = bhtLink.getHref().substring(bhtLink.getHref().indexOf("?pageId=") + 8);
                    String pageIdFromUrl = subStr.replaceAll("\\D+","");

                    Page pageTemp = pageManager.getPage(Long.parseLong(pageIdFromUrl));

                    String spaceKey2 = pageTemp.getSpaceKey();
                    String pageTitle2 = pageTemp.getTitle();

                    DefaultLinkBuilder linkBuilder = DefaultLink.builder();

                    PageResourceIdentifier identifier = new PageResourceIdentifier(spaceKey2, pageTitle2);

                    Link wiseLink = linkBuilder.withDestinationResourceIdentifier(identifier).build();

                    try {

                        String storageOfLink1 = xhtmlContent.convertLinkToStorage(wiseLink, context);

                        page.setBodyAsString(xhtmlBody.replace(bhtLink.getFullUrl(), storageOfLink1));


                    } catch (XhtmlException e) {
                        e.printStackTrace();
                    }

                }

                pageManager.saveNewVersion(page, new BhtModification(), DefaultSaveContext.DEFAULT);
            }

        }
    }
}
