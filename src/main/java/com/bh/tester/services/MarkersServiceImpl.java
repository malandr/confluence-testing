package com.bh.tester.services;

import com.atlassian.confluence.pages.Comment;
import com.bh.tester.models.BhtComment;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class MarkersServiceImpl implements MarkersService {

//    private String getMinMarker(Map<String, Integer> map, int minMarkerInt) {
//
//        for (Map.Entry<String, Integer> entry : map.entrySet()) {
//            if (entry.getValue() == minMarkerInt) return entry.getKey();
//        }
//
//        return "";
//    }
//
//    @Override
//    public Comment getFirstComment(List<Comment> comments, String body) {
//
//        Map<String, Integer> markersMap = new HashMap<>();
//
//        for (Comment comment : comments) {
//            if (comment.isInlineComment() && comment.getParent() == null && !comment.isDeleted() ) {
//                String marker = comment.getProperties().getStringProperty("inline-original-selection");
//                markersMap.put(marker, body.indexOf(marker));
//            }
//        }
//
////        Set<Map.Entry<String,Integer>> entries = markersMap.entrySet();
//
//        int minMarkerInt = 0;
//
//        for (Integer integer : markersMap.values()) {
//            if (integer < minMarkerInt) minMarkerInt = integer;
//        }
//
//        for (Comment comment : comments) {
//            String marker = comment.getProperties().getStringProperty("inline-original-selection");
//            if(marker.equals(getMinMarker(markersMap, minMarkerInt))) return comment;
//        }
//
//        return new Comment();
//    }

    @Override
    public Comment getFirstComment(List<Comment> comments, String body) {

        List<BhtComment> bhtComments = new ArrayList<>();

        for (Comment comment : comments) {

            if (comment.getParent() == null && !comment.isDeleted()) {
                BhtComment bhtComment = new BhtComment();
                bhtComment.setComment(comment);
                bhtComment.setMarker(comment.getProperties().getStringProperty("inline-original-selection"));
                bhtComment.setMarkerPosition(body.indexOf(bhtComment.getMarker()));
                bhtComments.add(bhtComment);
            }
        }

        List<Integer> ints = new ArrayList<>();

        for (BhtComment bhtComment : bhtComments) {
            ints.add(bhtComment.getMarkerPosition());
        }

        int min = Collections.min(ints);

        for (BhtComment bhtComment : bhtComments) {
            if (bhtComment.getMarkerPosition() == min) return bhtComment.getComment();
        }

        return new Comment();
    }
}
