package com.bh.tester.services;

import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.user.ConfluenceUser;

import java.util.Set;

public interface UsersExtractorService {

    Set<ConfluenceUser> extractUsers(Comment comment);
}
