package com.bh.tester.services;

import com.atlassian.confluence.pages.Comment;

import java.util.List;

public interface MarkersService {

    Comment getFirstComment(List<Comment> comments, String body);
}
