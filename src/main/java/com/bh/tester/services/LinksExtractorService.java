package com.bh.tester.services;

import com.atlassian.confluence.pages.Page;
import com.bh.tester.models.BhtLink;

import java.io.UnsupportedEncodingException;
import java.util.List;

public interface LinksExtractorService {

    List<BhtLink> getWebLinksFromPage(Page page, String hostUrl);

    void changeLinks(Page page, String hostUrl) throws UnsupportedEncodingException;
}