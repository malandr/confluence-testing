package com.bh.tester;

import org.jsoup.Jsoup;

public class HtmlToStringService {

    public static String convertHTMLtoText(String html) {

        return Jsoup.parse(html).text();
    }
}
