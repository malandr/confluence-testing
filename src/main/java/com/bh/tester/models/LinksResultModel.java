package com.bh.tester.models;

import com.atlassian.confluence.pages.Page;

import java.util.List;

public class LinksResultModel {

    private Page page;
//    private Map<OutgoingLink, Integer> linksMap;
    private List<BhtLinkOld> bhtLinkOlds;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public List<BhtLinkOld> getBhtLinkOlds() {
        return bhtLinkOlds;
    }

    public void setBhtLinkOlds(List<BhtLinkOld> bhtLinkOlds) {
        this.bhtLinkOlds = bhtLinkOlds;
    }
}
