package com.bh.tester.models;

import com.atlassian.confluence.pages.Comment;

public class BhtComment {

    private Comment comment;
    private int markerPosition;
    private String marker;

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public int getMarkerPosition() {
        return markerPosition;
    }

    public void setMarkerPosition(int markerPosition) {
        this.markerPosition = markerPosition;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }
}
