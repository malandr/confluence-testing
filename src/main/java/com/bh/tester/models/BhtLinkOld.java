package com.bh.tester.models;

import com.atlassian.confluence.links.OutgoingLink;
import com.atlassian.confluence.pages.Page;

public class BhtLinkOld {

    private Page page;
    private OutgoingLink outgoingLink;
    private String responseMessage;
    private int respCode;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public OutgoingLink getOutgoingLink() {
        return outgoingLink;
    }

    public void setOutgoingLink(OutgoingLink outgoingLink) {
        this.outgoingLink = outgoingLink;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public int getRespCode() {
        return respCode;
    }

    public void setRespCode(int respCode) {
        this.respCode = respCode;
    }
}
