package com.bh.tester;
/*
BeastieHut
quadr988@gmail.com
June 19, 2019
 */

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.web.context.HttpContext;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ConfluenceImport;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.inject.Inject;

@SuppressWarnings("ALL")
@Scanned
public class ConfluenceImports {

    @Inject
    public ConfluenceImports(// @ConfluenceImport PluginLicenseManager pluginLicenseManager,
                             // @ConfluenceImport LoginUriProvider loginUriProvider,
                             @ConfluenceImport PageManager pageManager,
                             @ConfluenceImport HttpContext httpContext,
                             @ConfluenceImport UserManager userManager,
                             @ConfluenceImport SpaceManager spaceManager,
                             @ConfluenceImport EventPublisher eventPublisher,
                             @ConfluenceImport PermissionManager permissionManager,
                             @ConfluenceImport SpacePermissionManager spacePermissionManager,
                             @ConfluenceImport UserAccessor userAccessor,
                             @ConfluenceImport XhtmlContent xhtmlContent,
                             @ConfluenceImport TemplateRenderer templateRenderer
                             ) {
    }
}
