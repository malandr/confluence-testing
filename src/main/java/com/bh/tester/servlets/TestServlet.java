package com.bh.tester.servlets;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.mail.notification.ConversionContextCreator;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.bh.tester.services.MarkersService;

import com.atlassian.templaterenderer.TemplateRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Scanned
public class TestServlet extends HttpServlet {

    private final PageManager pageManager;
    private final MarkersService markersService;
    private final XhtmlContent xhtmlContent;
    private final UserAccessor userAccessor;
    private final TemplateRenderer templateRenderer;

    public TestServlet(PageManager pageManager, MarkersService markersService,
                       XhtmlContent xhtmlContent, UserAccessor userAccessor, TemplateRenderer templateRenderer) {
        this.pageManager = pageManager;
        this.markersService = markersService;
        this.xhtmlContent = xhtmlContent;
        this.userAccessor = userAccessor;
        this.templateRenderer = templateRenderer;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Map<String, Object> context = new HashMap<>();

        ConversionContextCreator conversionContextCreator = new ConversionContextCreator();

        ConversionContext conversionContext = conversionContextCreator.createConversionContext(new Page());

        Page page = pageManager.getPage(3047426);

        String storageBody = xhtmlContent.convertWikiBodyToStorage(page).getBodyAsString();

        List<Integer> indexes = new ArrayList<Integer>();
        List<String> mentionedKeys = new ArrayList<String>();
//        List<UserKey> mentionedKeys = new ArrayList<>();
        List<String> storageFormats = new ArrayList<>();

        int index = 0;
        while(index != -1){
            index = storageBody.indexOf("ri:userkey=\"", index);
            if (index != -1) {
                indexes.add(index);
                index++;
            }
        }

        for (Integer tesmpIndex : indexes) {
            int start = tesmpIndex + 12;
            int end = start + 32;
            String mentioedKey = storageBody.substring(start, end);
            mentionedKeys.add(mentioedKey);
//            UserKey userKey = new UserKey(mentioedKey);
//            mentionedKeys.add(userKey);
        }

        for (String keyM : mentionedKeys) {
            try {
                String convertedStringB =
                        xhtmlContent.convertStorageToView("<ac:link><ri:user ri:userkey=\"" + keyM + "\" /></ac:link>", conversionContext);
                storageFormats.add(convertedStringB);
                context.put("storageFormats", storageFormats);
            } catch (XMLStreamException e) {
                e.printStackTrace();
            } catch (XhtmlException e) {
                e.printStackTrace();
            }
        }


        String convertedUserName = null;

        try {
            convertedUserName =
                    xhtmlContent.convertStorageToView("<ac:link><ri:user ri:userkey=\"8a80cb8169061b930169061dc1660000\" /></ac:link>", conversionContext);

        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (XhtmlException e) {
            e.printStackTrace();
        }
        context.put("convertedUserName", convertedUserName);
        context.put("writer", response.getWriter());
        context.put("mentionedKeys", mentionedKeys);
        context.put("indexes", indexes);

        templateRenderer.render("templates/servlet-template.vm", context, response.getWriter());


    }
}
