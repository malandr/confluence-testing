package com.bh.tester;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.model.links.DefaultLink;
import com.atlassian.confluence.content.render.xhtml.model.links.DefaultLinkBuilder;
import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.PageResourceIdentifier;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.xhtml.api.Link;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

//        List<String> webLinks = new ArrayList<>();
//
//        Document document = Jsoup.parse("<p><a href=\"https://en.wikipedia.org/wiki/Main_Page\">https://en.wikipedia.org/wiki/Main_Page</a></p>\n" +
//                "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>\n" +
//                "<p><a href=\"https://css-tricks.com/what-is-super-in-javascript/\">https://css-tricks.com/what-is-super-in-javascript/</a></p>\n" +
//                "<p><a href=\"http://localhost:8703/display/DES/Lorem\">http://localhost:8703/display/DES/Lorem</a></p>\n" +
//                "<p><ac:link><ri:page ri:content-title=\"Design Home\" /></ac:link></p>\n" +
//                "<p><ac:link /></p>\n" +
//                "<p><a href=\"http://localhost:8703/display/DES/Design+Home4444\">http://localhost:8703/display/DES/Design+Home4444</a></p>\n" +
//                "<p><a href=\"http://localhost:8613/conf/display/DOC/Page+2\">http://localhost:8613/conf/display/DOC/Page+2</a></p>\n" +
//                "<p><ac:structured-macro ac:name=\"tester\" ac:schema-version=\"1\" ac:macro-id=\"c69b398d-92cd-4528-8de4-48deee56d8a2\" /></p>");
//
//        Elements links = document.getElementsByTag("a");
//
//        for (Element link : links) {
//            webLinks.add(link.outerHtml());
//            System.out.println(link.outerHtml());
//        }

        String url = "http://localhost:8703/pages/viewpage.action?pageId=4620291&src=contextnavpagetreemode";

        String subStr = url.substring(url.indexOf("?pageId=") + 8);

        String subStrDigitsOnly = subStr.replaceAll("\\D+","");

        System.out.println(subStrDigitsOnly);

    }
}
